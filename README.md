# pyra




![](machine/pyra.png)


## Specs

````
Manufacturer	OpenPandora GmbH
Type	Handheld game console / PDA hybrid
Operating system	Debian
System on a chip	Texas Instruments OMAP5432
CPU	1.5 GHz dual-core ARM Cortex-A15 with NEON SIMD
Memory	4 GB
Storage	Dual SDXC slots, one internal microSD slot
Display	5" LCD resistive display 1280 × 720 (720p)
Graphics	533 MHz PowerVR SGX544MP2
Controller input	D-pad
Connectivity	Wi-Fi, Bluetooth, USB (all port sizes), MicroHDMI, headset port and 3G/4G/UMTS/GPS (optional)
Dimensions	139 mm x 87 mm x 32 mm
Mass	384 g
Predecessor	OpenPandora
````

Website	pyra-handheld.com

````
Dimensions	139 mm x 87 mm x 32 mm
SoC	Texas Instruments OMAP5432 (1.5 GHz dual-core ARM Cortex-A15, 2x ARM Cortex-M4, 533 MHz PowerVR SGX544MP2, Vivante GC320 2D Accelerator)
Memory	4GiB RAM, 32GB internal eMMC
Screen	5-inch LCD resistive display, 720p, 20 ms response time
Audio	High-quality speakers, analogue volume wheel, 3.5 mm headphone jack, built-in-mic
Haptic feedback	Force feedback vibramotor
Keyboard	Four-row backlit QWERTY keyboard
Game controls	D-pad, 6 face action buttons, 4 shoulder buttons, 2 clickable analog nubs
Ports	Dual SDXC full size slots + one microSD slot underneath the battery
Micro-USB 3.0 on-the-go (OTG) port
Full-size USB 2.0 host port
Full-size USB 3.0 host port (only 2.0 speed until a CPU upgrade) with optional eSATA adapter (full SATA speed)
HDMI output through Type D (micro) port
Battery	User-replaceable battery with 6000 mAh capacity
LEDs	Fully configurable RGB LEDs for notifications
Connectivity	Wi-Fi, Bluetooth 4.1	3G/4G module including GPS, a 6-axis accelerometer, digital compass, pressure and humidity sensors, Wi-Fi, Bluetooth 4.1
````


## NetBSD Project

https://github.com/NetBSD/src/blob/netbsd-9/sys/arch/evbarm/conf/GENERIC64#L20

https://github.com/NetBSD/src/blob/netbsd-9/sys/arch/evbarm/conf/GENERIC#L18




